export const environment = {
  production: true,
  loginPath: "http://localhost:8080/login",
  gradPath: "http://localhost:8080/grads",
  skillPath: "http://localhost:8080/skills",
  trendsPath: "http://localhost:8080/trends"
};
