import { Component, OnInit } from '@angular/core';
import { AuthService } from "angularx-social-login";
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private authService: AuthService, private router:Router) { }

  ngOnInit(): void {
    //console.log("hone init function ");
  }

  signOut(): void {
    this.authService.signOut();
    localStorage.clear();
    this.router.navigateByUrl("");
  }


}
