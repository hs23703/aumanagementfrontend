import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { AppModule } from 'src/app/app.module';
import { AuthService } from 'angularx-social-login';
import { Router } from '@angular/router';


describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let authService: AuthService;
  let router: Router
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
              AppModule ],
      declarations: [ HomeComponent],

    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
   // fixture.detectChanges();
  });
  

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should sign out',async(()=>{
   authService=TestBed.inject(AuthService);
   router=TestBed.inject(Router);
   spyOn(authService,"signOut");
   spyOn(router,"navigateByUrl");
   component.signOut();
   expect(localStorage.length).toBe(0);
  }))
  it('should ngOnInIt',()=>{
    expect(component.ngOnInit).toBeTruthy();
  })

});
