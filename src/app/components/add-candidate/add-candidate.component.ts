import { Component, OnInit,Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CandidatesService } from '../../services/candidates.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-add-candidate',
  templateUrl: './add-candidate.component.html',
  styleUrls: ['./add-candidate.component.scss']
})
export class AddCandidateComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  dropdownSettings :IDropdownSettings;

skills:any;
  constructor(private router:Router, private route:ActivatedRoute, private candidateService: CandidatesService,private loader:NgxSpinnerService) { }

  ngOnInit(): void {


    this.selectedItems = [
       {  id:1, name: 'java' },
    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    //console.log("cc");
    this.candidateService.getSkills().subscribe((skills)=>
      { 
        this.skills=skills;
        
        this.dropdownList = [
        ];
        for(let skill of skills)
        this.dropdownList.push(skill);
      }
    )
    ;
}

gradForm = new FormGroup({
  firstName: new FormControl(''),
  middleName: new FormControl(''),
  lastName:new FormControl(''),
  branch: new FormControl(''),
  contact: new FormControl('',[Validators.pattern('[0-9 ]*'),Validators.maxLength(10),Validators.minLength(10)]),
 degree: new FormControl(''),
 email: new FormControl('',[Validators.email]),
 gender: new FormControl(''),
 institute:new FormControl(''),
 joiningDate:new FormControl(''),
 joiningLocation:new FormControl(''),
 skills:new FormControl(''),
});
get firstName(){ return this.gradForm.get("firstName");}
get email(){ return this.gradForm.get("email");}
get lastName(){return this.gradForm.get("lastName");}
get contact(){return this.gradForm.get("contact");}
get joiningLocation(){return this.gradForm.get("joiningLocation");}
get joiningDate(){return this.gradForm.get("joiningDate");}
get gender(){return this.gradForm.get("gender");}

onSubmit() {
  this.gradForm.value.createdBy=localStorage.getItem('email');

  this.loader.show();
  this.candidateService.save(this.gradForm.value).subscribe((result)=>
  {
    this.loader.hide();
    this.router.navigateByUrl("/candidates")
  });
}



}
