import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddCandidateComponent } from './add-candidate.component';
import { AppModule } from 'src/app/app.module';
import { CandidatesService } from 'src/app/services/candidates.service';
import { observable, of } from 'rxjs';
import { Router } from '@angular/router';



describe('AddCandidateComponent', () => {
  let component: AddCandidateComponent;
  let fixture: ComponentFixture<AddCandidateComponent>;
  let service: CandidatesService;
  let router:Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[AppModule],
      declarations: [ AddCandidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCandidateComponent);
    component = fixture.componentInstance;
   //fixture.detectChanges();
  });
  it('should create',async( () => {
    expect(component).toBeTruthy();
  }));
  it('should check working of get  methods of gradForm',async( () =>{
    expect(component.firstName).toBeTruthy();
    expect(component.email).toBeTruthy();
    expect(component.lastName).toBeTruthy();
    expect(component.contact).toBeTruthy();
    expect(component.joiningDate).toBeTruthy();
    expect(component.joiningLocation).toBeTruthy();
    expect(component.gender).toBeTruthy();

  }));
  it('should check working of onSubmit method',async(()=>{
    service=TestBed.inject(CandidatesService);
    router=TestBed.inject(Router);
    spyOn(localStorage, 'getItem').and.returnValue("someValue");
    spyOn(service,"save").and.returnValue(of("something"));
    spyOn(router,"navigateByUrl");
    component.onSubmit();
    spyOn(service,"getSkills").and.returnValue(of("something"));
    component.ngOnInit();
    expect(component.skills).toBe("something");
  }))
  
});
