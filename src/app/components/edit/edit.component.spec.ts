import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditComponent } from './edit.component';
import { AppModule } from 'src/app/app.module';
import { CandidatesService } from 'src/app/services/candidates.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';

describe('EditComponent', () => {
  let component: EditComponent;
  let fixture: ComponentFixture<EditComponent>;
  let service: CandidatesService;
  let router:Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[AppModule],
      declarations: [ EditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });
  it('should create', () => {
   expect(component).toBeTruthy();
  });

  it('should check working of get  methods of gradForm',async( () =>{
    expect(component.firstName).toBeTruthy();
    expect(component.email).toBeTruthy();
    expect(component.lastName).toBeTruthy();
    expect(component.contact).toBeTruthy();
    expect(component.joiningDate).toBeTruthy();
    expect(component.joiningLocation).toBeTruthy();
    expect(component.gender).toBeTruthy();
  }));

  it('should check working of onSubmit method',async(()=>{
    let candidate={
      id:12,
      parentId:3,
    };
    component.candidate=candidate;
    service=TestBed.inject(CandidatesService);
    router=TestBed.inject(Router);
    spyOn(localStorage, 'getItem').and.returnValue("someValue");
    spyOn(service,"updatebyId" ).and.returnValue(of("something"));
    spyOn(router,"navigateByUrl");
    expect(component.onSubmit()).toBeUndefined();
  }))

  it('should check working of ngOnIt method',async(()=>{
    let skills=[
      {id:1,
      name:'java'},
      {id:2,
      name:'C++'}
    ];
    let candidate={
      id:12,
      parentId:3,
      branch:"",
      email:'',
      firstName:'',
      lastName:'',
      degree:'',
     contact:'',
     joiningDate:'',
     joiningLocation:'',
      skills:skills,
      gender:'',
    };
    history.pushState({data:candidate},"data");
    component.candidate=candidate;
    service=TestBed.inject(CandidatesService);
     spyOn(service,"getSkills").and.returnValue(of(skills));
     component.ngOnInit();
      expect(component.skills).toBe(skills);

  }))
 });
