import { Component, OnInit,Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CandidatesService } from '../../services/candidates.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  @Input()
  candidate=history.state.data;
  updatedCandidate;
  skills:any;
  selectedSkills=[];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings :IDropdownSettings;

  constructor(private router:Router, private route:ActivatedRoute, private candidateService: CandidatesService,private loader:NgxSpinnerService) { }

  ngOnInit(): void {

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };


    this.candidateService.getSkills().subscribe((skills)=>
    { 
      this.skills=skills;
      
      this.dropdownList = [
      ];
      for(let skill of skills)
      this.dropdownList.push(skill);
      this.gradForm.get('skills').setValue(this.candidate.skills);
    }
  );
    this.gradForm.get("firstName").setValue(this.candidate.firstName);
    this.gradForm.get("middleName").setValue(this.candidate.middleName);
    this.gradForm.get("lastName").setValue(this.candidate.lastName);
    this.gradForm.get("branch").setValue(this.candidate.branch);
    this.gradForm.get("degree").setValue(this.candidate.degree);
    this.gradForm.get("contact").setValue(this.candidate.contact);
    this.gradForm.get("email").setValue(this.candidate.email);
    this.gradForm.get("joiningDate").setValue(this.candidate.joiningDate);
    this.gradForm.get("joiningLocation").setValue(this.candidate.joiningLocation);
    this.gradForm.get("gender").setValue(this.candidate.gender);
    this.gradForm.get("institute").setValue(this.candidate.institute);
    // console.log(this.candidate);
 
   
}


gradForm = new FormGroup({
  firstName: new FormControl('',[Validators.required]),
  middleName: new FormControl(''),
  lastName:new FormControl(''),
  branch: new FormControl(''),
  contact: new FormControl('',[Validators.pattern('[0-9 ]*'),Validators.maxLength(10),Validators.minLength(10)] ),
  degree: new FormControl(''),
  email: new FormControl('',Validators.email),
 gender: new FormControl(''),
 institute:new FormControl(''),
 joiningDate:new FormControl(''),
 joiningLocation:new FormControl(''),
 skills:new FormControl('')
});
get firstName(){ return this.gradForm.get("firstName");}
get email(){ return this.gradForm.get("email");}
get lastName(){return this.gradForm.get("lastName");}
get contact(){return this.gradForm.get("contact");}
get joiningLocation(){return this.gradForm.get("joiningLocation");}
get joiningDate(){return this.gradForm.get("joiningDate");}
get gender(){return this.gradForm.get("gender");}




onSubmit():void {
  this.gradForm.value.createdBy=localStorage.getItem('email');
  this.gradForm.value.id=this.candidate.id;
  this.gradForm.value.parentId=this.candidate.parentId;
this.loader.show();
  this.candidateService.updatebyId(this.gradForm.value,this.candidate.id).subscribe((result)=>
  {
    this.loader.hide();
    this.router.navigateByUrl("/candidates")
  });

}
}
