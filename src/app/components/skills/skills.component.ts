import { Component, OnInit } from '@angular/core';
import { CandidatesService } from '../../services/candidates.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {
  skills: any;
  constructor( private candidateService: CandidatesService, private router:Router) { }

  ngOnInit(): void {
    this.candidateService.getSkills().subscribe((skills)=> 
    {
      console.log(skills);
      this.skills=skills;
    }
    )
  }

//skill form for skill object
  skillForm=new FormGroup({
    name: new FormControl(''),
  });

  onSubmit()
{
  this.candidateService.saveSkill(this.skillForm.value).subscribe((result)=>
  {
    console.log(result);
    this.router.navigateByUrl("/candidates")
  });
}

}
