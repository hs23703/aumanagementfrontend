import { Component, OnInit } from '@angular/core';
import { AuthService } from 'angularx-social-login';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(private authService: AuthService, private router:Router)  { }

  ngOnInit(): void {
  }
  signOut(): void {
    this.authService.signOut();
    localStorage.clear();
    this.router.navigateByUrl("");
  }

}
