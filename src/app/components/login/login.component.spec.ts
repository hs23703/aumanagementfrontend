import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { AppModule } from 'src/app/app.module';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { SocialUser, AuthService } from 'angularx-social-login';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let loginservice: LoginService;
  let router:Router;
  let user= new SocialUser;
  user.idToken="sfdfdfdf";
  user.email="dfdfdf";
  let mockAuthService ={
    authState: of(user)
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[AppModule],
      declarations: [ LoginComponent ],
      providers:[{AuthService, useValue: mockAuthService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should tryForLogin with valid creditials',async(()=>{
  let user= new SocialUser;
  user.idToken="sfdfdfdf";
  user.email="abc@mail.com";
  component.userData=user;
  loginservice=TestBed.inject(LoginService);
  router=TestBed.inject(Router);
  spyOn(loginservice,"makeRequest").and.returnValue(of(true));
  spyOn(router,"navigateByUrl");
  component.tryForLogin(user)
  expect(localStorage.getItem('email')).toBe('abc@mail.com');
}));
it('should tryForLogin with inValid creditials',async(()=>{
  let user= new SocialUser;
  user.idToken="sfdfdfdf";
  user.email="abc@mail.com";
  component.userData=user;
  loginservice=TestBed.inject(LoginService);
  router=TestBed.inject(Router);
  spyOn(localStorage,"setItem");
  spyOn(router,"navigateByUrl");
  spyOn(loginservice,"makeRequest").and.returnValue(of(false));
  component.tryForLogin(null);
  component.tryForLogin(user);
  expect(component.ngOnInit()).toBeUndefined();

}));

// it('should be able to signin',async(()=>{
//   let user= new SocialUser;
//   user.idToken="sfdfdfdf";
//   user.email="dfdfdf";
//  mockAuthService=TestBed.inject(AuthService);
//  spyOn(component,"tryForLogin");
//  expect(component.signInWithGoogle()).toBeUndefined();
// }));

})
