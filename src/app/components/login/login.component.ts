import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleLoginProvider } from "angularx-social-login";
import { AuthService } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { LoginService } from '../../services/login.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {  
  userData: SocialUser;

  constructor(private authService: AuthService, private httpservice: LoginService,private router:Router,private loader:NgxSpinnerService) { }

  ngOnInit() { 

  }

  signInWithGoogle(): void {
    this.loader.show();
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(userData => {
      this.tryForLogin(userData);
 });

  }

 tryForLogin(userData:any):void{
  if(userData!==null)
  { 
    this.httpservice.makeRequest(userData).subscribe(result => {
      if(result===true)
      {
        this.loader.hide();
        localStorage.setItem('token', userData.idToken );
        localStorage.setItem('email',userData.email);
        this.router.navigateByUrl('/candidates');
    }
    else 
    {
      this.loader.hide();
      this.router.navigateByUrl("/");
    }    
  });
    }
    else
   { this.loader.hide();
    this.router.navigateByUrl("/");
   }
 }

}

