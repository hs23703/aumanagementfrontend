import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CandidatesService } from '../../services/candidates.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-candidate-detail',
  templateUrl: './candidate-detail.component.html',
  styleUrls: ['./candidate-detail.component.scss']
})
export class CandidateDetailComponent implements OnInit {

  @Input() 
  candidate:any;
  constructor(private router:Router, private route:ActivatedRoute, private candidateService: CandidatesService,private loader:NgxSpinnerService) { }

  ngOnInit(): void {
   let id = +this.route.snapshot.paramMap.get('id');
    this.loader.show();
    this.candidateService.getById(id).subscribe((candidate)=>{
      this.loader.hide();
      this.candidate=candidate;
    });

  
  }

}

