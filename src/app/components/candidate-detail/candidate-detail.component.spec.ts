import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CandidateDetailComponent } from './candidate-detail.component';
import { AppModule } from 'src/app/app.module';
import { CandidatesService } from 'src/app/services/candidates.service';
import { of } from 'rxjs';

describe('CandidateDetailComponent', () => {
  let component: CandidateDetailComponent;
  let fixture: ComponentFixture<CandidateDetailComponent>;
  let candidateService: CandidatesService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[AppModule],
      declarations: [ CandidateDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateDetailComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
   expect(component).toBeTruthy();
  });
  it("should get the detail of the candidate", async(()=>  {
    candidateService=TestBed.inject(CandidatesService);
    spyOn(candidateService,"getById").and.returnValue(of("something"));
    component.ngOnInit();
    expect(component.candidate).toBe("something");
}))
});
