import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-edit-btm',
  templateUrl: './edit-btm.component.html',
  styleUrls: ['./edit-btm.component.scss']
})
export class EditBtmComponent implements  ICellRendererAngularComp {

 
  public params: any;
  agInit(params: any): void {
    this.params = params;
   // console.log(params.data);
  }

  refresh(): boolean {
    return false;
  }

  goToEdit()
  {
    this.params.context.componentParent.goToEdit(this.params.data)
  }
goToDetail()
{
  let id =this.params.data.id;
  //console.log(id);
  this.params.context.componentParent.details(id);
}
delete()
{

  let id =this.params.data.id;
  //console.log("delete "+ id);
  this.params.context.componentParent.delete(id);
}

}
