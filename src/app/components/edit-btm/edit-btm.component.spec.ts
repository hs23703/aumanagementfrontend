import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBtmComponent } from './edit-btm.component';
import { AppModule } from 'src/app/app.module';

describe('EditBtmComponent', () => {
  let component: EditBtmComponent;
  let fixture: ComponentFixture<EditBtmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[AppModule],
      declarations: [ EditBtmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBtmComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
   expect(component).toBeTruthy();
  });
  it('should test methods of edit-btm',async(()=>{
   let params={
     data:{
       id:1
     },
     context:{
       componentParent:{ 
         goToEdit(){},
         details(){},
         delete(){}
      }
     }
    };
    component.agInit(params);
    spyOn(component.params.context.componentParent,"goToEdit");
    component.goToEdit();
    component.goToDetail();
    component.delete();
    component.refresh();
    expect(component.params.data.id).toBe(1);
  }))
});
