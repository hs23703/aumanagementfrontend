import { Component, OnInit } from '@angular/core';
import {CandidatesService} from '../../services/candidates.service';
import { Router } from '@angular/router';
import {EditBtmComponent} from '../edit-btm/edit-btm.component'
import { NgxSpinnerService } from 'ngx-spinner';
import { LOADERS } from 'ngx-spinner/lib/ngx-spinner.enum';

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.scss']
})
export class CandidatesComponent implements OnInit {

  candidates: any;
  selectedCandidate: any;
  rowData:any;
  context = { componentParent: this };
  frameworkComponents = {
    editBtmComponent: EditBtmComponent,};
  gridOptions;



  columnDefs = [
    {headerName: 'ID',  sortable: true , field: "parentId",resizable: true,maxWidth:100},
    {headerName: 'Name',  sortable: true, valueGetter: function(params) {
      return params.data.firstName +" "+ params.data.lastName;
    },resizable: true,},
    {headerName: 'Email', field: 'email', sortable: true, resizable: true,maxWidth:200},
    {headerName: "Location", field: "joiningLocation", sortable: true,resizable: true, maxWidth:100},
   {headerName:"Contact", field: "contact", sortable:true, resizable: true, maxWidth: 150 },
   { 
      cellRenderer: 'editBtmComponent', resizable: true
   }
];


  constructor(private candidateService: CandidatesService,private router:Router, private loader:NgxSpinnerService) { 
  }

  ngOnInit(): void {
    this.loader.show()
    this.candidateService.getcandidates().subscribe((candidates)=> 
    {
      this.loader.hide();
      this.candidates=candidates;
      this.rowData=candidates;
    }
    );
  }

  details(id:number){
     this.router.navigateByUrl("details/"+id);
  }

  delete(id:number)
  {
    this.loader.show();
   this.candidateService.deleteById(id).subscribe((result)=>
    {
      this.loader.hide();
      window.location.reload()
      this.router.navigateByUrl("/candidates");
    });
    
  }

  goToEdit(candidate:any)
  {
    this.loader.show()
    this.candidateService.getById(candidate.id).subscribe((candidate)=>
    {
      this.loader.hide();
      this.router.navigate(['/edit'], { state: { data: candidate } });
    })

  }


  onRowClicked(event: any) {
    //this.selectedCandidate=event.data;
    // console.log('row', event.data); 
  }

}


