import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CandidatesComponent } from './candidates.component';
import { AppModule } from 'src/app/app.module';
import { CandidatesService } from 'src/app/services/candidates.service';
import { of } from 'rxjs';
import { Router } from '@angular/router';

describe('CandidatesComponent', () => {
  let component: CandidatesComponent;
  let fixture: ComponentFixture<CandidatesComponent>;
  let candidateService: CandidatesService;
  let router:Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule ],
      declarations: [ CandidatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatesComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
   it('Check Grids static values', () => {
   expect(component.columnDefs[0].headerName).toBe("ID");
   expect(component.context.componentParent).toBe(component);
   expect(component.columnDefs[1].headerName).toBe("Name");
   expect(component.columnDefs[0].resizable).toBe(true);
 });
 it('should test ngOnInIt and Crud operation',async(() =>{
      candidateService=TestBed.inject(CandidatesService);
      router=TestBed.inject(Router);
      spyOn(router,"navigateByUrl");
      spyOn(candidateService,'getcandidates').and.returnValue(of("any"));
      spyOn(candidateService,'deleteById').and.returnValue(of('amy'));
      spyOn(candidateService,'getById').and.returnValue(of({id:12}));
      component.goToEdit({id:12});
      component.details(2)
      component.ngOnInit();
      expect(component.candidates).toBe('any');
 }))

});
