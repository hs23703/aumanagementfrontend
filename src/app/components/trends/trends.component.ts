import { Component, OnInit } from '@angular/core';
import { CandidatesService } from '../../services/candidates.service';
import { NgModel } from '@angular/forms';
import {Chart} from 'chart.js'

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.scss']
})
export class TrendsComponent implements OnInit {
  result=[];
  year;
  type;
  chart : Chart
  name=[];
  count=[];

  constructor(private candidatesService:CandidatesService) { }

  ngOnInit(): void {
  }

  getTrend()
  {
    let year=this.year;
    let type=this.type;
     this.candidatesService.getTrend( year, type).subscribe((result)=>{
      // console.log(result);
       this.result=JSON.parse(result);; 


      this.name=[];
      this.count=[];
       for(let x of this.result)
        {
          this.name.push(x.name);
          this.count.push(x.count);
        }

      this.chart = new Chart( 'canvas', {
        type: 'bar',

        data: {
          labels: this.name,
          datasets: [
            {
              label: 'Number of Grads',
              data: this.count,
              backgroundColor: '#3F51B5',
              borderColor: '#3F51B5',
              borderWidth: 1
            },
          ],
        },

        options: { 
          responsive: true,
          legend: {
            position: 'top',
          },
          title:{

            display: true,
            text: year+':Number of Grads '+type+'wise'
          },
          scales:{

            yAxes:[
              {ticks:{beginAtZero:true,stepSize: 1}}
            ]
          }
         },
      });
    });


  }
      
  

}
