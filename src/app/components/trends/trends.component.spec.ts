import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendsComponent } from './trends.component';
import { AppModule } from 'src/app/app.module';
import { CandidatesService } from 'src/app/services/candidates.service';
import { of } from 'rxjs';

describe('TrendsComponent', () => {
  let component: TrendsComponent;
  let fixture: ComponentFixture<TrendsComponent>;
  let candidatesService: CandidatesService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[AppModule],
      declarations: [ TrendsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendsComponent);
    component = fixture.componentInstance;
   // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should get Trend',async(()=>{
    const mockTrend=[{
      name:'location1',
      count:2 },
    {
    name: 'location2',
    count:3 }
  ];
    candidatesService=TestBed.inject(CandidatesService);
    spyOn(candidatesService,"getTrend").and.returnValue(of(JSON.stringify(mockTrend)));
    component.getTrend();
    expect(component.result).toEqual(mockTrend);
  }))
});
