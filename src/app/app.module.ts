import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CandidatesComponent } from './components/candidates/candidates.component';
import { CandidateDetailComponent } from './components/candidate-detail/candidate-detail.component';
import { FormsModule }    from '@angular/forms';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, AuthService  } from "angularx-social-login";
import { HttpClientModule } from '@angular/common/http';
import { EditComponent } from './components/edit/edit.component';
import { SkillsComponent } from './components/skills/skills.component';
import { TrendsComponent } from './components/trends/trends.component';  
import { ReactiveFormsModule } from '@angular/forms';
import { AddCandidateComponent } from './components/add-candidate/add-candidate.component';
import { NavComponent } from './components/nav/nav.component';
import { AgGridModule } from 'ag-grid-angular';
import { EditBtmComponent } from './components/edit-btm/edit-btm.component';
import { NgxSpinnerModule } from "ngx-spinner";  
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('286959256087-6afke0oct4p0vuleh2uabjl22baev2rj.apps.googleusercontent.com')
  }
]);

export function provideConfig() {
  return config;
}  

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CandidatesComponent,
    CandidateDetailComponent,
    EditComponent,
    SkillsComponent,
    TrendsComponent,
    AddCandidateComponent,
    NavComponent,
    EditBtmComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    SocialLoginModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([EditBtmComponent]),
    NgxSpinnerModule,
    BrowserAnimationsModule,
    NgMultiSelectDropDownModule.forRoot()
    
    
  ],
  providers: [  
    AuthService,  
    {  
      provide: AuthServiceConfig,
      useFactory: provideConfig 
    }  
  ],  
  bootstrap: [AppComponent]
})
export class AppModule { }
