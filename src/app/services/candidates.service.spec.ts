import { TestBed } from '@angular/core/testing';
import { CandidatesService } from './candidates.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('CandidatesService', () => {
  let service: CandidatesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        CandidatesService,
      ],
    });
 
    service = TestBed.get(CandidatesService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() =>
  {
    httpMock.verify();
  });
  

  it('should be created', () => {
   expect(service).toBeTruthy();
  });

  it('should check working of get grads api',()=>{
    const gradsMock=[
      {
        id:1,
        firstName:'Gurharmanjit'
      },
      {
        id:2,
        firstName:'Taranjit'
      }
    ];
    service.getcandidates().subscribe(grads=>{
    expect(grads.length).toBe(2);
    expect(gradsMock).toEqual(grads);
    })
    const request=httpMock.expectOne(environment.gradPath);
    expect(request.request.method).toBe('GET');
    request.flush(gradsMock);
  })

  it('should check working of get grad by id api',()=>{
    const mockGrad={
      id:1,
      firstName:'Gurharmanjit'
    }
    service.getById(1).subscribe(grad=>{
      expect(grad.id).toBe(1);
    })
    const request=httpMock.expectOne(environment.gradPath+"/1");
    expect(request.request.method).toBe('GET');
    request.flush(mockGrad);
  })

  it('should be able to check save api',()=>{
    const mockGrad={
      id:1,
      firstName:'Gurharmanjit'
    }
    service.save(mockGrad).subscribe(grad=>{
      expect(grad.id).toBe(1);
    })
    const request=httpMock.expectOne(environment.gradPath);
    expect(request.request.method).toBe('POST');
    request.flush(mockGrad);
  })

  it('should be able to check update By id api',()=>{
    const mockGrad={
      id:1,
      firstName:'Gurharmanjit'
    }
    service.updatebyId(mockGrad,1).subscribe(grad=>{
      expect(grad.id).toBe(1);
    })
    const request=httpMock.expectOne(environment.gradPath+"/1");
    expect(request.request.method).toBe('PUT');
    request.flush(mockGrad);
  })

  it('should be able to check delete id api',()=>{
    const mockGrad={
      id:1,
      firstName:'Gurharmanjit'
    }
    service.deleteById(1).subscribe(grad=>{
      expect(grad.id).toBe(1);
    })
    const request=httpMock.expectOne(environment.gradPath+"/1");
    expect(request.request.method).toBe('DELETE');
    request.flush(mockGrad);
  })

  it('should be able to get skills',()=>{
    const mockSkills=[{
      id:1,
      name:'C++'
    },
    {id:2,
    name: 'java'
  }];
    service.getSkills().subscribe(skills=>{
      expect(skills.length).toBe(2);
    })
    const request=httpMock.expectOne(environment.skillPath);
    expect(request.request.method).toBe('GET');
    request.flush(mockSkills);
  })

  it('should be able to get the trends',()=>{
    const mockTrends=[{
      name:'location1',
      count:2 },
    {
    name: 'location2',
    count:3 }
  ];
    service.getTrend("2020","location").subscribe(trends=>{
      let parsed=JSON.parse(trends);; 
      expect(parsed.length).toBe(2);
    })
    const request=httpMock.expectOne(environment.trendsPath+"?year=2020&type=location");
    expect(request.request.method).toBe('GET');
    request.flush(mockTrends);
  })
  it('should be able to check save skill api',()=>{
    const mocksSkill={ name:'Java'};
    service.saveSkill(mocksSkill).subscribe(skill=>{
      expect(skill).toEqual(mocksSkill);
    })
    const request=httpMock.expectOne(environment.skillPath);
    expect(request.request.method).toBe('POST');
    request.flush(mocksSkill);
  })

});
