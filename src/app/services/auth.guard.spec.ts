import { TestBed } from '@angular/core/testing';
import { AuthGuard } from './auth.guard';
import { Router } from '@angular/router';

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let routerMock = {navigate: jasmine.createSpy('navigate')}

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard, { provide: Router, useValue: routerMock },],
    });
 guard = TestBed.inject(AuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
  it('should not  direct unauthorized user to login',() => {
    localStorage.clear();
    expect(guard.canActivate()).toBe(false);
  })
  it('should let the authorized user login',()=>{
  spyOn(localStorage, 'getItem').and.returnValue("someValue");
   expect(guard.canActivate()).toBe(true);
  })

});
