import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CandidatesService {

  constructor(private http:HttpClient) { }
  getcandidates() : Observable<any> {

    let url= environment.gradPath;
    return this.http.get(url, {headers: {token: localStorage.getItem("token") } } ) ;
  }

  getById(id:number): Observable<any>
  {
    let url=environment.gradPath;
    url=url+"/"+id;
      return this.http.get(url,{headers: {token: localStorage.getItem("token") } });
  }
  getSkills() : Observable<any>
  {
    let url=environment.skillPath;
        return this.http.get(url,{headers: {token: localStorage.getItem("token") } });
  }
  updatebyId(grad:any,id:number,):Observable<any>
  {
    let url=environment.gradPath;;
    url=url+"/"+id;
    return this.http.put(url,grad,{headers: {token: localStorage.getItem("token") } });
  }
  save(grad:any):Observable<any>
  {
    let url=environment.gradPath;;
    return this.http.post(url,grad,{headers: {token: localStorage.getItem("token") } });
  }
  deleteById(id:number):Observable<any>
  {
    let url=environment.gradPath;;
    url=url+"/"+id;
    return this.http.delete(url,{headers: {token: localStorage.getItem("token") } }); 
  }
  saveSkill(skill:any){
    let url=environment.skillPath;
    return this.http.post(url,skill,{headers: {token: localStorage.getItem("token") } } );
  }

  getTrend(year,type)
  {
    let url=environment.trendsPath; 
    return this.http.get(url, {  params: {
      year: year,
      type: type,
    }, responseType: 'text', headers: {token: localStorage.getItem("token") } } );

  }

}
