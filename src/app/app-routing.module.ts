import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CandidatesComponent } from './components/candidates/candidates.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { CandidateDetailComponent } from './components/candidate-detail/candidate-detail.component';
import { EditComponent } from './components/edit/edit.component';
import { SkillsComponent } from './components/skills/skills.component';
import { TrendsComponent } from './components/trends/trends.component';
import { AddCandidateComponent } from './components/add-candidate/add-candidate.component';
import { AuthGuard } from './services/auth.guard';


const routes: Routes = [
  { path: "home", component: HomeComponent,canActivate: [AuthGuard]},
  { path: "candidates" ,component: CandidatesComponent,canActivate: [AuthGuard]},
  { path: "", component: LoginComponent},
  { path: 'details/:id', component: CandidateDetailComponent,canActivate: [AuthGuard]},
  {path: "edit", component: EditComponent,canActivate: [AuthGuard]},
  {path: "skills", component: SkillsComponent,canActivate: [AuthGuard]},
  {path: "trends", component: TrendsComponent,canActivate: [AuthGuard]},
  {path: "add-candidate", component: AddCandidateComponent,canActivate: [AuthGuard]},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


// { path: 'detail/:id', component: HeroDetailComponent },